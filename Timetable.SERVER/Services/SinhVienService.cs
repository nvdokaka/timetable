﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timetable.SERVER.Protos;
using Timetable.SERVER.Repositories;

namespace Timetable.SERVER.Services
{
    public class SinhVienService : SinhVien.SinhVienBase
    {
        public override Task<SinhVienData> GetSinhVien(RequestMaSo request, ServerCallContext context)
        {
            SinhVienData sv = SinhVienRepository.TT_GET_INFO_SV(request.Maso);
            return Task.FromResult(sv);
        }
        public override Task<SinhVienData> LoginSV(RequestLogin request, ServerCallContext context)
        {
            SinhVienData sv = SinhVienRepository.TT_LOGIN_SV(request.Username, request.Password);
            return Task.FromResult(sv);
        }
        
    }
}
